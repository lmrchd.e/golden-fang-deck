# golden-fang-deck

Those are images for use in roll-20 macros with the following text:
```
[[ [ ](https://gitlab.com/lmrchd.e/golden-fang-deck/-/raw/main/folder/[[ 1dX ]].png)
```

I DO NOT OWN most of the images in this repo and will link to the source everytime. It's for my personal use only (I will put them in the roll-20 chat), but for the macro to work the repo must be public. If you do not like the use of your picture and wish for me to remove it, feel free to message me.